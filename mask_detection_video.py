from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from imutils.video import VideoStream
import time
import numpy as np
import argparse
import cv2

# adding parser
parser = argparse.ArgumentParser()
parser.add_argument("-m", "--model", type=str, default="./models/model_nas_net_mobile", help="Mask detector model")
parser.add_argument("-v", "--video", type=str, default="output.avi", help="Mask detector model")
args = vars(parser.parse_args())

# loading trained model
print("[I] Loading trained model")
model = load_model(args["model"])

# load the cascade
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# loading video stream
vs = VideoStream(src=0).start()
time.sleep(2.0)

# for video saving
out = cv2.VideoWriter(args["video"], cv2.VideoWriter_fourcc('M','J','P','G'), 10, (640,480))

while True:
    # load frame
    frame = vs.read()

    imageToDisplay = frame.copy()

    # convent into grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # detect faces
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        # some preprocessing
        image = imageToDisplay[y:y + h, x:x + w]
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = cv2.resize(image, (224, 224))
        image = img_to_array(image)
        image = preprocess_input(image)
        image = np.expand_dims(image, axis=0)

        # apply image to model
        (mask, withoutMask) = model.predict(image)[0]

        # setting label and color
        label = "Mask" if mask > withoutMask else "Without mask"
        # setting color
        color = (0, 225, 0) if label == "Mask" else (0, 0, 225)

        # format text
        label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)

        cv2.rectangle(imageToDisplay, (x, y), (x + w, y + h), color, 2)
        cv2.putText(imageToDisplay, label, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)

    # show result
    cv2.imshow("Result", imageToDisplay)

    # write the flipped frame
    out.write(imageToDisplay)

    key = cv2.waitKey(33)
    if key == ord("q"):
        break

# clean
out.release()
cv2.destroyAllWindows()
vs.stop()