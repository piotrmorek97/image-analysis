# import the necessary packages
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import NASNetMobile
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras import Sequential
from tensorflow.keras.optimizers import RMSprop, Adam
from tensorflow.keras.losses import BinaryCrossentropy
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.applications.nasnet import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.utils import to_categorical
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
import cv2

# constants
IMG_SIZE = 224

# Load data from folder to arrays
print("[I] Loading data from directory...")
data = []
labels = []

path = "./dataset"
filesPath = []

# load the cascade
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# get all filepaths
for dirpath, dirnames, filenames in os.walk(path):
    listOfFiles = [os.path.join(dirpath, file) for file in filenames]
    for file in listOfFiles:
        filesPath.append(file)

# get data and labels
for imagePath in filesPath:
    # get name of directory for label
    label = imagePath.split(sep='\\')[-2]

    # loading input image, preprocess
    loadedImage = cv2.imread(imagePath)
    image = loadedImage.copy()

    # convent into grayscale
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # detect faces
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        image = loadedImage[y:y + h, x:x + w]
        image = cv2.resize(image, (224, 224))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        hist = cv2.calcHist([image], [0], None, [256], [0, 256])
        # add data to lists
        data.append(hist)
        labels.append(label)

# converting data and labels to numpy arrays
data = np.array(data, dtype='float32')
labels = np.array(labels)

# one hot encoding on labels
labelBinarizer = LabelBinarizer()
labels = labelBinarizer.fit_transform(labels)
labels = to_categorical(labels)

# train, test split
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.25, stratify=labels, random_state=42)

# # data image generator for data augumentation
# datagen = ImageDataGenerator(
# 		rotation_range=25,
# 		zoom_range=0.2,
# 		width_shift_range=0.15,
# 		height_shift_range=0.15,
# 		shear_range=0.15,
# 		horizontal_flip=True,
# 		fill_mode="nearest")

IMG_SHAPE = (IMG_SIZE, IMG_SIZE, 1)

model = Sequential([
    Dense(512, activation="relu", input_shape=(256, 1)),
    Dense(256, activation="relu"),
    Dense(16, activation="relu"),
    Dense(2, activation="softmax")
])

# # add them to model
# model = Sequential([
#     Conv2D(128, 3, padding="same", activation="relu", input_shape=IMG_SHAPE),
#     MaxPooling2D(),
#     Dropout(0.2),
#     Conv2D(128, 3, padding="same", activation="relu"),
#     MaxPooling2D(),
#     Dropout(0.45),
#     Flatten(),
#     Dense(64, activation="relu"),
#     Dense(2, activation="softmax")
# ])

model.summary()

# set lr and epochs
base_learning_rate = 0.0001

# compile model
model.compile(optimizer=Adam(lr=base_learning_rate),
              loss=BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])

# constants
BS = 32
EPOCHS = 4

# train the head of the network
print("[I] training network...")
history = model.fit(
	steps_per_epoch=len(trainX) // BS,
	validation_data=(testX, testY),
	validation_steps=len(testX) // BS,
	epochs=EPOCHS)

# serialize the model to disk
print("[I] saving model...")
# model.save(args["model"], save_format="h5")
model.save("./models/model_own_network", save_format="h5")

# plot the training loss and accuracy
N = EPOCHS

loss = history.history["loss"]
val_loss = history.history["val_loss"]

acc = history.history["accuracy"]
val_acc = history.history["val_accuracy"]

plt.figure(1)
plt.plot(range(0, N), loss, label="Training loss")
plt.plot(range(0, N), val_loss, label="Validation loss")
plt.title("Training, validation loss")
plt.ylabel("Loss")
plt.xlabel("Epochs")
plt.legend(loc="upper right")

plt.savefig("./plots/training_val_loss_figure_2")

plt.figure(2)
plt.plot(range(0, N), acc, label="Training accurancy")
plt.plot(range(0, N), val_acc, label="Validation accurancy")
plt.title("Training, validation accurancy")
plt.ylabel("Accurancy")
plt.xlabel("Epochs")
plt.legend(loc="upper left")

plt.savefig("./plots/training_acc_figure_2")



