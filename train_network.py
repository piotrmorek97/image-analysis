# import the necessary packages
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications import NASNetMobile
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.layers import Dropout
from tensorflow.keras import Sequential
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.losses import BinaryCrossentropy
from tensorflow.keras.layers import Dense
from tensorflow.keras.applications.nasnet import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.utils import to_categorical
from sklearn.preprocessing import LabelBinarizer
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os

# constants
IMG_SIZE = 224

# Load data from folder to arrays
print("[I] Loading data from directory...")
data = []
labels = []

path = "./dataset"
filesPath = []

# get all filepaths
for dirpath, dirnames, filenames in os.walk(path):
    listOfFiles = [os.path.join(dirpath, file) for file in filenames]
    for file in listOfFiles:
        filesPath.append(file)

# get data and labels
for imagePath in filesPath:
    # get name of directory for label
    label = imagePath.split(sep='\\')[-2]

    # loading input image, preprocess
    image = load_img(imagePath, target_size=(IMG_SIZE, IMG_SIZE))
    image = img_to_array(image)
    image = preprocess_input(image)

    # add data to lists
    data.append(image)
    labels.append(label)

# converting data and labels to numpy arrays
data = np.array(data, dtype='float32')
labels = np.array(labels)

# one hot encoding on labels
labelBinarizer = LabelBinarizer()
labels = labelBinarizer.fit_transform(labels)
labels = to_categorical(labels)

# train, test split
(trainX, testX, trainY, testY) = train_test_split(data, labels, test_size=0.25, stratify=labels, random_state=42)

# data image generator for data augumentation
datagen = ImageDataGenerator(
		rotation_range=15,
		zoom_range=0.10,
		width_shift_range=0.3,
		height_shift_range=0.3,
		shear_range=0.15,
		horizontal_flip=True,
		fill_mode="nearest")

IMG_SHAPE = (IMG_SIZE, IMG_SIZE, 3)

# Create the base model from the pre-trained model MobileNet V2
base_model = NASNetMobile(input_shape=IMG_SHAPE,
                          include_top=False,
                          weights='imagenet')

base_model.trainable = False

# add our layers
global_average_layer = GlobalAveragePooling2D()
dense_layer_1 = Dense(128, activation="relu")
dropout_layer = Dropout(0.25)
final_layer = Dense(2, activation="softmax")

# add them to model
model = Sequential([
  base_model,
  global_average_layer,
  dense_layer_1,
  dropout_layer,
  final_layer
])

model.summary()

# set lr and epochs
base_learning_rate = 0.0001

# compile model
model.compile(optimizer=RMSprop(lr=base_learning_rate),
              loss=BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])

# constants
BS = 32
EPOCHS = 4

# train the head of the network
print("[I] training network...")
history = model.fit(
	datagen.flow(trainX, trainY, batch_size=BS),
	steps_per_epoch=len(trainX) // BS,
	validation_data=(testX, testY),
	validation_steps=len(testX) // BS,
	epochs=EPOCHS)

# serialize the model to disk
print("[I] saving model...")
# model.save(args["model"], save_format="h5")
model.save("./models/model_nas_net_mobile", save_format="h5")

# plot the training loss and accuracy
N = EPOCHS

loss = history.history["loss"]
val_loss = history.history["val_loss"]

acc = history.history["accuracy"]
val_acc = history.history["val_accuracy"]

plt.figure(1)
plt.plot(range(0, N), loss, label="Training loss")
plt.plot(range(0, N), val_loss, label="Validation loss")
plt.title("Training, validation loss")
plt.ylabel("Loss")
plt.xlabel("Epochs")
plt.legend(loc="upper right")

plt.savefig("./plots_nas_net_mobile/training_loss_figure")

plt.figure(2)
plt.plot(range(0, N), acc, label="Training accurancy")
plt.plot(range(0, N), val_acc, label="Validation accurancy")
plt.title("Training, validation accurancy")
plt.ylabel("Accurancy")
plt.xlabel("Epochs")
plt.legend(loc="lower right")

plt.savefig("./plots_nas_net_mobile/training_acc_figure")



