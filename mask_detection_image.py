from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
import numpy as np
import argparse
import cv2
import os

# adding parser
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--image", default="./examples/example_03.png", help="Path to input image")
parser.add_argument("-m", "--model", type=str, default="./models/model_nas_net_mobile", help="Mask detector model")
args = vars(parser.parse_args())

# loading trained model
print("[I] Loading trained model")
model = load_model(args["model"])

# load the cascade
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

# loading image
image = cv2.imread(args["image"])
imageToDisplay = image.copy()
(h, w) = image.shape[:2]

# convent into grayscale
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# detect faces
faces = face_cascade.detectMultiScale(gray, 1.3, 5)

# drawing faces
for (x, y, w, h) in faces:
    # some preprocessing
    image = imageToDisplay[y:y+h, x:x+w]
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (224, 224))
    image = img_to_array(image)
    image = preprocess_input(image)
    image = np.expand_dims(image, axis=0)

    # apply image to model
    (mask, withoutMask) = model.predict(image)[0]

    # setting label and color
    label = "Mask" if mask > withoutMask else "Without mask"
    # setting color
    color = (0, 225, 0) if label == "Mask" else (0, 0, 225)

    # format text
    label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)

    cv2.rectangle(imageToDisplay, (x, y), (x + w, y + h), color, 2)
    cv2.putText(imageToDisplay, label, (x, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)

# path to save picture
filename = args['image'].split('/')[-1]
path = os.path.join('./analyzed_pictures', 'analyzed_' + filename)
print(path)

cv2.imshow("Result", imageToDisplay)
cv2.imwrite(path, imageToDisplay)
cv2.waitKey(0)
